<?php

use App\Http\Controllers\FormController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\Module\ItemController;
use App\Http\Controllers\MyController;
use App\Http\Controllers\UserController;
use App\Jobs\TestJob;
use App\Models\Item;
use App\Models\User;
use Illuminate\Support\Facades\Route;
use PhpParser\Node\Expr\Cast\Double;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    // dd(opcache_get_status()['jit']);
    // die;

    $var = "<h1>hello world</h1>";
    $test = 'yes';

    return view('welcome',['var'=> $var , 'test' => $test]);
    return view('welcome',compact('var','test'));
});

Auth::routes();

Route::middleware(['auth'])->group(function(){
    Route::name('staff.')->prefix('module_staff')->middleware(['checkAge'])->group(function(){
        Route::view('testing_page','testing')->name('testing_page_name');

        Route::get('pass/{nric}/parameter',function($nric){
            echo $nric;
        })->name('pass-parameter');
    });

    Route::resource('user',UserController::class);

});





Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');


Route::get('route-to-controller/{name}', [HomeController::class,'record'])->name('record');
// Route::get('argument',function(){

// });

// function insert_data(Array $data,String $string ,Double $harga)
// {

// }


Route::get('name_argument', function () {
    $string = 'hello &#47;';
    return htmlspecialchars($string, 'UTF-8', false);
    return htmlspecialchars(string: $string, encoding: 'UTF-8', double_encode: false);

});

Route::get('constructor', function () {
    TestJob::dispatch(5.0,2.0);
});

Route::get('union', function () {
    Class Number{
        public function __construct(
            private int|float $number
        )
        {}
    }
    new Number('NaN');
});



Route::get('null_safe', function () {
  $data = User::find(321312);

//   if($data){
//     echo $data->email;
//   }

    // echo $data ? $data->email : 'null';

    echo $data?->email.'<br>';
    echo optional($data)->email;
});

Route::get('match_expression', function () {
    switch (8.0) {
        case '8.0':
          $result = "Oh no!";
          break;
        case 8.0:
          $result ="This is what I expected";
          break;
      }
      echo $result;


    echo match (8.0) {
        '8.0' => "Oh no!",
        8.0 => "This is what I expected",
      };

 });


Route::get('object_class',function(){
    class conversation{}

    $obj = new conversation;

    $type = '';
    switch (get_class ($obj)) {
        case 'conversation':
        $type = "started_conversation";
        break;

        case "Reply":
        $type = "replied_to_conversation";
        break;

        case 'Comment':
        $type = "commented_on_lesson";
        break;
    }

    // return $type;


    switch ($obj::class) {
        case 'conversation':
        $type = "started_conversation";
        break;

        case "Reply":
        $type = "replied_to_conversation";
        break;

        case 'Comment':
        $type = "commented_on_lesson";
        break;
    }

    return $type;
});

Route::get('new_helper_a',function(){
    $id = 'inv_a1b3243b52z';
    $id2 = 'a1b3243b52z_inv';

    dd(!! preg_match('/_inv$/',$id2) ,  str_ends_with($id2,'_inv'));
    // dd(stripos($id,'inv_')=== 0, str_starts_with($id,'inv_'));



});

Route::get('new_helper_b',function(){
    $url = 'https://example.com?foo=bar';
    dd(strpos($url, '?')  !== false, str_contains($url,'?'));
});

Route::get('model',function(){
    $data = Item::get();
    //select * from items

    $search = 'mobi';
    $data = App\Models\Item::where('item_name','LIKE','%'.$search.'%')->first();
    //select * from items where item_name LIKE %mobi% limit 1

    $data = Item::whereHas('item_description',function($q){
        $q->where('colour','red');
    })->first();

    $data = User::where('name','LIKE','%'.'firdaus'.'%')->first();

    foreach($data->items as $list){
        echo '<br>';
        echo $list?->item_name.'<br>';
        echo $list?->price.'<br>';
        echo optional(optional($list->item_description))->colour.'<br>';
        echo $list?->item_description?->description.'<br>';
        echo '------------------------------------------';
    }
    die;

    // dd($data->item_description->colour.' '.$data->item_description->description);
});

Route::get('form',[FormController::class,'form'])->middleware('auth')->name('form');

Route::post('form-validate',[FormController::class,'store'])->middleware('auth')->name('form-validate');

Route::get('item/index',[ItemController::class,'index'])->middleware('auth')->name('item.index');
Route::get('item/create',[ItemController::class,'create'])->middleware('auth')->name('item.create');
Route::get('item/edit/{id}',[ItemController::class,'edit'])->middleware('auth')->name('item.edit');
Route::post('item/destroy/{id}',[ItemController::class,'destroy'])->middleware('auth')->name('item.destroy');
Route::post('item/store',[ItemController::class,'store'])->middleware('auth')->name('item.store');
Route::post('item/update/{id}',[ItemController::class,'update'])->middleware('auth')->name('item.update');
