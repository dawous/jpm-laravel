@extends('template.app')
@section('title','Item List')
@section('content')
<div class="float-right">
    <a href="{{route('item.create')}}" class="btn btn-primary"><i class="fa fa-plus"></i>  Item</a>
</div>
<div class="row mb-2">
    <div class="col-md-1">
        <select class="form-control" id="size" onchange="table_size(this.value)">
            <option @if(request()->size == 5) selected @endif value ="5">5</option>
            <option @if(request()->size == 10) selected @endif value ="10">10</option>
            <option @if(request()->size == 25) selected @endif value ="25">25</option>
        </select>
    </div>
    <div class="col-md-11">
        <div class="input-group mb-3">
            <input type="text" placeholder="Carian" id="search" value="{{request()->search}}" class="form-control" onkeydown="search(this.value)">
            <div class="input-group-append">
              <div class="input-group-text">
                <span class="fas fa-search"></span>
              </div>
            </div>
        </div>
    </div>
</div>
<div class="mb-2">
    <small>
    Papar <b>{{($items->currentpage()-1)*$items->perpage()+1}}</b> hingga <b>{{$items->currentpage()*$items->perpage()}}</b>
    daripada  <b>{{$items->total()}}</b> senarai
    </small>
</div>
<div class="table-responsive">
    <table class="table">
        <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Item Name</th>
                <th scope="col">Price (RM)</th>
                <th scope="col">Item Description</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            @forelse ($items as $key => $item)
                <tr>
                    <th>{{$items->firstItem() + $key }}</th>
                    <td>{{$item->item_name}}</td>
                    <td>{{number_format($item->price,2)}}</td>
                    <td>
                        Colour: {{$item?->item_description?->colour ?? '-'}}<br>
                        Code: {{$item?->item_description?->code ?? '-'}}<br>
                        Description: {{$item?->item_description?->description ?? '-'}}<br>
                    </td>
                    <td>
                        <a href="{{route('item.edit',Crypt::encryptString($item->id))}}" class="btn btn-info"><i class="fa fa-edit"></i></a>
                        <a href="javascript:void(0);" data-id="#delete-{{$item->id}}" data-title="{{$item->item_name}}" class="btn btn-danger confirmation"><i class="fa fa-trash"></i></a>
                        <form id="delete-{{$item->id}}" action="{{route('item.destroy',Crypt::encryptString($item->id))}}" method="post">
                            @csrf
                        </form>
                    </td>
                </tr>
            @empty
                <tr>
                    <td colspan='5'>No record</td>
                </tr>
            @endforelse
        </tbody>
    </table>
    {{ $items->withQueryString()->links('vendor.pagination.bootstrap-4')}}
</div>
@endsection
@push('js')
<script>
    function table_size(size)
    {
        var search = $("#search").val();
        window.location.replace("/item/index?search="+ search +"&size=" + size);
    }

    function search(search)
    {
        var size = $("#size").val();
        if(event.key === 'Enter') {
            window.location.replace("/item/index?search=" + search + "&size=" + size);
        }
    }

    confirmation_dialog();

    function confirmation_dialog()
        {
            $( ".confirmation" ).click(function() {
                var id = $(this).data('id');
                var title = $(this).data('title');

                // console.log(id);

                Swal.fire({
                    title: 'Confirmation',
                    html: "Are you sure you want to remove <b>"+title.toUpperCase()+"</b>? You won't be able to revert this!",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#6259ca',
                    cancelButtonColor: '#f16d75',
                    confirmButtonText: 'Yes, delete it!'
                }).then((result) => {
                    if (result.value) {
                        $(id).submit();
                    }
                });
            })
        }
</script>
@endpush
