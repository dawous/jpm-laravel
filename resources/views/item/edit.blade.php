@extends('template.app')
@section('title','Update Item')
@section('content')
    <form action="{{route('item.update',Crypt::encryptString($item->id))}}" method="post">
        @csrf
        @include('item.component.form')
        <input type="submit" class="btn btn-primary" value="update">
    </form>
@endsection
