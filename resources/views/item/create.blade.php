@extends('template.app')
@section('title','Add Item')
@section('content')
    <form action="{{route('item.store')}}" method="post">
        @csrf
        @include('item.component.form')
        <input type="submit" class="btn btn-primary" value="submit">
    </form>
@endsection
