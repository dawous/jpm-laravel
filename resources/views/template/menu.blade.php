<nav class="mt-2">
    <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
      <!-- Add icons to the links using the .nav-icon class
           with font-awesome or any other icon font library -->
      <li class="nav-item">
        <a href="#" class="nav-link ">
          <i class="nav-icon fas fa-tachometer-alt"></i>
          <p>
            Route
            <i class="right fas fa-angle-left"></i>
          </p>
        </a>
        <ul class="nav nav-treeview">
          <li class="nav-item">
            <a href="{{ route('staff.testing_page_name') }}" class="nav-link active">
              <i class="far fa-circle nav-icon"></i>
              <p>Testing Page</p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{ url('module_staff/testing_page') }}" class="nav-link">
              <i class="far fa-circle nav-icon"></i>
              <p>Testing Page 2</p>
            </a>
          </li>
          <li class="nav-item">
            @php
                $nric = '900821055441';
            @endphp
            <a href="{{ route('staff.pass-parameter',$nric) }}" class="nav-link">
              <i class="far fa-circle nav-icon"></i>
              <p>Pass Parameter</p>
            </a>
          </li>
        </ul>
      </li>
      <li class="nav-item">
        <a href="{{route('record',Auth::user()->name ?? 'Nama')}}" class="nav-link">
          <i class="nav-icon fas fa-th"></i>
          <p>
            Record
            {{-- <span class="right badge badge-danger">New</span> --}}
          </p>
        </a>
      </li>
      <li class="nav-item">
        <a href="{{route('item.index')}}" class="nav-link">
          <i class="nav-icon fas fa-list"></i>
          <p>
            Item
            {{-- <span class="right badge badge-danger">New</span> --}}
          </p>
        </a>
      </li>
    </ul>
  </nav>
