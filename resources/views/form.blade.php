@extends('template.app')
@section('title','Home Page')
@section('content')
    <form action="{{route('form-validate')}}" method="post">
        @csrf
        <div class="form-group">
            <label>Item Name:</label>
            <input type="text" name="item_name" value="{{old('item_name')}}" class="form-control @error('item_name') is-invalid @enderror">
            @error('item_name')
                <span class="error invalid-feedback">{{$message}}</span>
            @enderror
        </div>

        <div class="form-group">
            <label>Price:</label>
            <input type="text" name="price" value="{{old('price')}}" class="form-control @error('price') is-invalid @enderror">
            @error('price')
                <span  class="error invalid-feedback">{{$message}}</span>
            @enderror
        </div>

        <input type="submit" class="btn btn-primary">
    </form>
@endsection
