<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ItemSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('items')->truncate();
        DB::table('items')->insert([
        	'item_name' => 'mobile',
        	'price' => 1000,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);
        DB::table('items')->insert([
            'item_name' => 'laptop',
            'price' => 2000,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);

    }
}
