<?php

namespace App\Http\Requests;

use Illuminate\Contracts\Encryption\DecryptException;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Validation\Rule;

class ItemUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        try {
        	$id = Crypt::decryptString($this->id);
        } catch (DecryptException $e) {
            abort(404);
        }

        return [
            'item_name' => ['required','string','max: 20', Rule::unique('items','item_name')->ignore($id,'id')],
            'price' => ['required' , 'regex:/^\d*(\.\d{2})?$/'],
        ];
    }
}
