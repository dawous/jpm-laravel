<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ItemStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'item_name' => 'required|string|max: 20|unique:items',
            'price' => ['required' , 'regex:/^\d*(\.\d{2})?$/'],
        ];
    }

    public function messages()
    {
        return [
            // 'required' => 'Ruang :attribute perlu diisi'
        ];
    }

    public function attributes()
    {
        return [
            // 'item_name' => 'product name'
        ];
    }
}
