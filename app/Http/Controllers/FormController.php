<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class FormController extends Controller
{
    public function form()
    {
        return view('form');
    }

    public function store(Request $request)
    {
        // dd(request()->all());

        $rules = [
            'item_name' => 'required|string|max: 20',
            'price' => ['required' , 'regex:/^\d*(\.\d{2})?$/'],
        ];

        $messages = [
            // 'required' => 'Ruang :attribute perlu diisi'
        ];

        $attributes = [
            'item_name' => 'nama produk'
        ];

        /*---------------php8 method ------------*/
        $this->validate(request: $request, rules: $rules,messages: $messages,customAttributes: $attributes);
        /*---------------end php8 method ------------*/


        //$this->validate($request,$rules,$messages,$attributes);



        //process save
    }
}
