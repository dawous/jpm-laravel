<?php

namespace App\Http\Controllers\Module;

use App\Http\Controllers\Controller;
use App\Http\Requests\ItemStoreRequest;
use App\Http\Requests\ItemUpdateRequest;
use App\Models\Item;
use App\Models\ItemDescription;
use Illuminate\Contracts\Encryption\DecryptException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Session;

class ItemController extends Controller
{
    public function index()
    {
        $size = request()->filled('size') ?  request()->size : 5;

        $items = Item::when(request()->filled('search'),function($q){
            $q->where(function($q){
                $q->where('item_name','LIKE','%'.request()->search.'%');
            });
        })->paginate($size);

        return view('item.index',compact('items'));

        // return view('item.index',['items' => $items]);
    }

    public function create()
    {
        $item = new Item();
        return view('item.create',compact('item'));
    }

    public function store(ItemStoreRequest $request)
    {
        $item = new Item();
        $item->item_name = $request->item_name;
        $item->price = $request->price;
        $item->user_id = Auth::user()->id;
        $item->save();

        $item_description = new ItemDescription();
        $item_description->item_id = $item->id;
        $item_description->description = "This is product description";
        $item_description->colour = "Blue";
        $item_description->code = "A123";
        $item_description->save();

        return redirect()->route('item.index')->with('status',['type'=> 'success','message'=> 'New item successfully saved.']);
    }

    public function edit($id)
    {
        try {
        	$id = Crypt::decryptString($id);
        } catch (DecryptException $e) {
            abort(404);
        }

        $item = Item::findOrFail($id);
        return view('item.edit',compact('item'));
    }

    public function update(ItemUpdateRequest $request, $id)
    {
        try {
        	$id = Crypt::decryptString($id);
        } catch (DecryptException $e) {
            abort(404);
        }

        $item = Item::findOrFail($id);
        $item->item_name = $request->item_name;
        $item->price = $request->price;
        $item->user_id = Auth::user()->id;
        $item->save();

        return redirect()->route('item.index')->with('status',['type'=> 'success','message'=> 'Item '.$item->item_name.' successfully updated.']);
    }

    public function destroy($id)
    {
        try {
        	$id = Crypt::decryptString($id);
        } catch (DecryptException $e) {
            abort(404);
        }

        $data = Item::findOrFail($id);
        $data->delete(); //delete item
        $data->item_description()?->delete(); //delete relationship data in table item_description

        return redirect()->route('item.index')->with('status', ['type' => 'success', 'message' => 'Item successfully deleted.']);
    }
}
