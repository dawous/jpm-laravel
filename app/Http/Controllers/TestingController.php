<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Spatie\RouteAttributes\Attributes\Get;

class TestingController extends Controller
{
    #[Get('route-saya')]
    public function myMethod()
    {
        return "mohamad firdaus";
    }
}
