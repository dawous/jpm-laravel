<?php

namespace App\Http\Controllers;

use App\Http\Middleware\RedirectIfAuthenticated;
use Illuminate\Http\Request;
use Spatie\RouteAttributes\Attributes\Get;

class MyController extends Controller
{
    #[Get('my-route', middleware: RedirectIfAuthenticated::class)]
    public function myMethod()
    {
        return 'hello world';
    }
}
